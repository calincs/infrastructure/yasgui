const express = require('express');
const app = express();
const port = 5000;

// handlebars
const handlebars = require('express-handlebars');
app.set('view engine', 'hbs');
app.engine('hbs', handlebars({
  layoutsDir: __dirname + '/views/layouts',
  partialsDir: __dirname + '/views/partials/',
  extname: 'hbs',
  defaultLayout: 'index'
}));

app.use(express.static('public'))
app.get('/', (req, res) => {
  // res.render('main', { layout: 'index' });
  res.render('main');
});

app.listen(port, () => console.log('App listening to port ${port}'));